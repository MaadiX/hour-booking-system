<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
   wp_enqueue_style( 'child-style', get_stylesheet_directory_uri().'/style.css' );

   // custom js 
    wp_enqueue_script( 'custom-scipts', get_stylesheet_directory_uri().( '/js/jquery.icunstoScripts.js' ), array( 'jquery' ), '2.1.2', true );
}
function child_setup() {
    remove_filter( 'wp_enqueue_scripts', 'twentyseventeen_scripts' );
}
add_action( 'after_setup_theme', 'child_setup' );


//Remove dashboard widgets for subscribers
add_action( 'wp_dashboard_setup', 'wp_dashboard_remove_widgets' );
 
/**
 * Remove core widgets and add widget to Dashboard in third column.
 * @see https://developer.wordpress.org/reference/functions/add_meta_box/
 */
function wp_dashboard_remove_widgets() {
    global $current_user; 
	wp_get_current_user();
    // Remove Welcome panel.
    if ( !user_can( $current_user, "administrator" ) ){ 
      remove_action( 'welcome_panel', 'wp_welcome_panel' );
 
      // Remove all Dashboard widgets.
      global $wp_meta_boxes;
      unset( $wp_meta_boxes['dashboard'] );
    }
 
}
// remove app booking page fr non admin
    function ft_remove_menus(){
    global $current_user; 
        wp_get_current_user();
    // Remove Welcome panel.
    if ( !user_can( $current_user, "administrator" ) ){      
        remove_menu_page( 'index.php' );                  //Dashboard
        remove_menu_page( 'cp_apphourbooking' );
      }
    }

    add_action( 'admin_menu', 'ft_remove_menus' ); 
/**
 * Reigister custom query vars.
 */
function themeslug_query_vars( $qvars ) {
    $qvars[] = 'norooms';
    return $qvars;
}
add_filter( 'query_vars', 'themeslug_query_vars' );
/* Define variables */
/* TODO: It would be useful to have a option page where to define these values
 * So the code could be used for several forms
 * */

/* Create Fellow User Role */
add_role(
    'fellow', //  System name of the role.
    __( 'Fellow'  ), // Display name of the role.
    array(
        'read'  => true,
	'list_users'	=> true,
	'edit_users'	=>true,
	'edit_user'	=> true,
	'remove_users'	=> true,
	'add_users'	=> true,
	'delete_users'	=> true,
	'manage_own_users'=> true,	
	'create_users'	=> true,
	//'edit_posts' => true,
	//'edit_pages'	=>true,
        'promote_user'  => true,
	'edit_user_meta' => true,
    )
);


function remove_higher_levels($all_roles) {
    $user = wp_get_current_user();
    $next_level = 'level_' . ($user->user_level + 1);

    foreach ( $all_roles as $name => $role ) {
        if (isset($role['capabilities'][$next_level])) {
            unset($all_roles[$name]);
        }
    }

    return $all_roles;
}

add_filter('editable_roles', 'remove_higher_levels');

add_action( 'user_register', 'fellows_registration_save', 10, 1 );

function fellows_registration_save( $user_id ) {
    global $current_user;
    wp_get_current_user();
    // Add meta to know who created the user
    if ( !user_can( $current_user, "administrator" ) ){
        update_user_meta($user_id, 'created_by',$current_user->ID );
    }

}

add_filter('pre_get_users', 'filter_users_for_fellows_list');

function filter_users_for_fellows_list($query)
{
 global $pagenow;
    global $current_user;
    wp_get_current_user();
    // Add meta to know who created the user
    if ( !user_can( $current_user, "administrator" ) ){

   // change the meta query based on who created user
   $meta_query = array (array (
      'key' => 'created_by',
      'value' => $current_user->ID,
      'compare' => 'LIKE'
   ));
   $query->set('meta_query', $meta_query);
  }
 }
// Avoid fellow users deleting/editing users they have not cretaed
function fellow_edit_permission_check() {
    global $current_user, $profileuser;
 
    $screen = get_current_screen();
    //get_currentuserinfo(); 
 wp_get_current_user();
    // Edit
    if( !user_can( $current_user->ID, "administrator" )   && in_array( $screen->base, array( 'user-edit', 'user-edit-network' ) ) ) { // editing a user profile
	$created_by = get_user_meta($profileuser->ID,'created_by', true);
        if ( !user_can( $current_user->ID, "administrator" ) and $created_by != $current_user->ID) { // trying to edit a superadmin while less than a superadmin
            wp_die( __( 'Oooooop! What are you doing?' ) );
        }
    }
  // Delete
  if( !user_can( $current_user->ID, "administrator" )   && in_array( $screen->base, array( 'users' ) )){
	$action =  isset($_GET['action'])? $_GET['action']:false;
	$delete_id =  isset($_GET['user'])? $_GET['user']:false;
	$created_by = get_user_meta($delete_id,'created_by', true);
        if ( !user_can( $current_user->ID, "administrator" ) and $created_by != $current_user->ID and $action=='delete') { // trying to edit a superadmin while less than a superadmin
            wp_die( __( 'Oooooop! What are you doing?' ) );
        }   
    }    
}
add_filter( 'admin_head', 'fellow_edit_permission_check', 1, 4 );

// change Menu lables
//add_action( 'admin_menu', 'change_user_label' );
function change_user_label(){
  global $menu, $submenu;
  $menu[70][0] = 'Usuarias';
  $submenu['users.php'][5][0] = 'Ver Todas';
  $submenu['user-new.php'][10][0] = 'Añadir nueva';
}
define( 'MAX_STREAMS', 2500 );
define( 'MARGEN_VIDEO_USERS', 50 );
define( 'AUDIO_USERS_FIELDNAME', 'fieldname4' );
define( 'VIDEO_USERS_FIELDNAME', 'fieldname3' );
define( 'MAX_CAPACITY', 10);

/* Hook to the App Bookin pluging.
 * Check availability before approvation based on total streams 
 * We are assuming that mx streams allowed is 800, defined in MAX_STREAMS
 */
add_action( 'cpappb_process_data_before_insert', 'custom_cp_validation', 10,1 );

function custom_cp_validation($data){
  global $wpdb;
  $form_id= $data['formid'];
  $formname=  $data['formname'];
  $count=count($data['apps']);
  $app_duartion = $data['apps'][0]['duration'];
  //Just get the startime to check if is available
  $app_statrtime = $data['app_starttime_1'];
  $app_useres_camara = (int)$data[''. VIDEO_USERS_FIELDNAME .''];
  $app_useres_audio = (int)$data[''. AUDIO_USERS_FIELDNAME .''];
  $current_book_streams=calculate_total_streams($app_useres_camara,$app_useres_audio);
// get all bookde dates
  $all_booked_days = array();
  //Build query to get all booking id by dates
    for ($i = 0; $i <$count ; $i++) {
      $app_days = $data['apps'][$i]['date'];
      if(!in_array($app_days, $all_booked_days)){
	$all_booked_days[]=$app_days;
      }
    }
    
  $regex_string= "SELECT id FROM wp_cpappbk_messages WHERE formid=%d AND (";
  $countDiff=count($all_booked_days);
  for ($i = 1; $i <=$countDiff ; $i++) {
	$regex_string.= "posted_data REGEXP 's:4:\"date\";s:10:\"" . $all_booked_days[$i-1]  . "\"'";
	if ($i<$countDiff)$regex_string.="OR ";
  }
  $regex_string.=")"; 
/* First query: just check if some date matches. if not we can accept bookig
 * TODO: We are assuming people will make a booking for a single day.
 * This check against data base should be improved, so taht we could aslo return a more detailed result about 
 * which date/slot is not available i f there is a reservation with nultiple date
 * */
//$myrows_ids = $wpdb->get_results( $wpdb->prepare("SELECT id FROM wp_cpappbk_messages WHERE formid=%d and posted_data REGEXP 's:4:\"date\";s:10:\"" . $data['apps'][0]['date'] . "\"'", $form_id), ARRAY_A );
  $myrows_ids = $wpdb->get_results( $wpdb->prepare($regex_string, $form_id), ARRAY_A );
  if ($myrows_ids) {

  /* If there is some match, cheack each booked slot to see if some is not available
   * Create a string with all the times slots from submitted data to use in mysql query
   * The pligin has large serialized fileds to store this info. Can not make an elegnat query :-(
   *
   */ 
  // Create simple array with ids
  $ids=array();
  foreach ($myrows_ids as $value){
      $ids[]=$value['id'];
  }
  $all_current_slots = array();
  $regex_string= "SELECT posted_data FROM wp_cpappbk_messages WHERE id IN (" . implode(',', array_map('intval', $ids)) . ") AND (";
  for ($i = 1; $i <=$count ; $i++) {
        
        $app_slot=$data['app_starttime_'. $i .''];
        $regex_string .=  "posted_data REGEXP 's:15:\"app_starttime_.*\";s:5:\"" . $app_slot . "\"' ";
	if ($i<$count)$regex_string.="OR ";
        //Create an array with all slots from current booking to late close slots inf needed
	$day_time_slot= array
		('day' => $data['apps'][$i-1]['date'],
		'time' => $app_slot,
		'quant' => $data['apps'][$i-1]['quant']
		);
        //$all_current_slots[]= $day_time_slot;
	$all_current_slots[]=$data['apps'][$i-1];
  } 
  $regex_string.=")";   
  $slot_query = $wpdb->get_results( $regex_string, ARRAY_A); 	
  // If some slot matches, get participnts and calculate streams to sum with current booking 
  if ($slot_query){
  $tot_streams = (int)0;
  /*TODO: Following code could give false positive, as we are getting a sum of all booked dates
   * We shoul better do a for loop, and when a total stream result + current book stream is bigger than
   * the max allowed, stop the loop, becuase booking can not be performes
   * */
  foreach ($slot_query as $value) {
          $value = unserialize($value['posted_data']);
          $no_camara = (int)$value['' . AUDIO_USERS_FIELDNAME. ''];
          $yes_camara = (int)$value['' . VIDEO_USERS_FIELDNAME . ''];
          $streams= calculate_total_streams($yes_camara,$no_camara);
          $tot_streams = $tot_streams + $streams; 
        }   
        $system_total_streams = $current_book_streams + $tot_streams;

        if ($system_total_streams > MAX_STREAMS) {
          // Don to allow booking
          $obj_id = get_queried_object_id();
          $current_url = get_permalink( $obj_id );
          wp_safe_redirect( add_query_arg( array( 'norooms' => 'true' ), $current_url ) );
          die();
        }else if ($system_total_streams < MAX_STREAMS and $system_total_streams + MARGEN_VIDEO_USERS > MAX_STREAMS){
          //Process booking an close future bookings . 
          // Get total bookings 
          // $slot_query is an array with all previous booking which matches date and some slot
          // $all_current_slots : array with all selected slots in current booking
	$slots_to_lock=array();
        foreach ($data['apps'] as $app_data) {
            $lock=false;
	    $tot_slot_streams = 0;
	    $day = $app_data['date'];
	    $time = $app_data['slot'];
	    $quant =  $app_data['quant'];		
            $available_slot_quantity = MAX_CAPACITY - $quant;
	    foreach ($slot_query as $result){		
	      $result = unserialize($result['posted_data']);
	      $c = count($result['apps']);
		for ($i = 1; $i <=$c ; $i++) { 
		    $app_slot_time=$result['apps'][$i-1]['slot'];
		    $app_slot_day = $result['apps'][$i-1]['date'];
		    if ( $app_slot_time ==  $time and $app_slot_day == $day){  

		      /* found a slot which matches with current bokking slot
                      * Get total stream for the slot and
		      * save fields values to later insert the slot data 
                       */

		      $slot_no_camara = (int)$result[''. AUDIO_USERS_FIELDNAME .''];
		      $slot_yes_camara = (int)$result[''. VIDEO_USERS_FIELDNAME .''];
		      $slot_quantity = (int)$result['app_quantity_'. $i .''];
		      $tot_streams_in_slot= calculate_total_streams($slot_yes_camara, $slot_no_camara);
                      $tot_slot_streams = $tot_slot_streams + $tot_streams_in_slot;
                      // Quantity is needed in order to know the number for blocking the slot if needed
		      $available_slot_quantity = $available_slot_quantity -  $slot_quantity;
		  }
		} // end for $i

			$app_data['quant'] = $available_slot_quantity;
			$app=$app_data;

	      } //end foreach $slot_query

	    // Sum current users
	    $tot_slot_streams = $tot_slot_streams + $current_book_streams;
            // This give us the total quantity boooked for this slot
            // BBB allows 800 sumultaneous streams. let's do some maths
            
	    if ($tot_slot_streams <= MAX_STREAMS and $tot_slot_streams + MARGEN_VIDEO_USERS > MAX_STREAMS){
		// If total allowed quantity from setting  would be x we will create a block entry with x - $quantity - 1
		// becuase in our slot_query , the last inserted item is not included.
                      $lock=true;
                      $apps[]=$app;
		// That way, if somebody cancel a bookin, its slot would be available again 
	
	    }
        } // end foerach all_current_slot 
	
	 // If there is some slot to be closed, create a blocked-slot entry with a quantity higer than the max, so the slot will be shown as unvailable

	if($lock) {
	  $slots_to_lock[]=array(
		  'formid'=>  $form_id,
		  'formname' => $formname,
		  'apps'     => $apps
	  );
		block_date_slot($slots_to_lock);
	}

      } // end if $system_total_users_video < MAX_VIDEO_USERS
    } // end if $slot_query
  } // end $myrows_ids
} // end function

function block_date_slot($slots_to_lock) {
	global $wpdb;
	$bookingTuned = new CP_AppBookingPlugin();
        $founddata = false;
        $params = array();
        $params["final_price"] = "";
        $data_date='';
        foreach($slots_to_lock as $entry){
	  $apps=$entry['apps'];
	  $params["apps"] = $apps;
	  foreach ($apps as $appitem)
        {
           $params["app_service_".$appitem["id"]] = $appitem["service"];
           $params["app_status_".$appitem["id"]] = $appitem["cancelled"];
           $params["app_duration_".$appitem["id"]] = $appitem["duration"];
           $params["app_price_".$appitem["id"]] = $appitem["price"];
           $params["app_date_".$appitem["id"]] = $appitem["date"];
           $params["app_slot_".$appitem["id"]] = $appitem["slot"];
           $slotpieces = explode("/",$appitem["slot"]);
           $params["app_starttime_".$appitem["id"]] = $bookingTuned->format12hours(trim(@$slotpieces[0]), @$appitem["military"] == 0);
           $params["app_endtime_".$appitem["id"]] = $bookingTuned->format12hours(trim(@$slotpieces[1]), @$appitem["military"] == 0);
           $params["app_quantity_".$appitem["id"]] = $appitem["quant"];
           $founddata = true;
           $data_date.= " Date: " . $appitem["date"] . " Time: " . $appitem["slot"] . " Quantity: " . $appitem["quant"];
        }

	  $params["formid"] = $entry['formid'];
	  $params["formname"] = $entry['formname'];
	  $params["referrer"] = "";
	} //  foreach($slots_to_lock as $entry)
        $data = "Reserva: " . $data_date;
        $rows_affected = $wpdb->insert( $wpdb->prefix.$bookingTuned->table_messages, array( 'formid' =>  $entry['formid'],
                                                                                    'time' => current_time('mysql'),
                                                                                    'ipaddr' => '',
                                                                                    'notifyto' => 'BLOCKED@BY.ADMIN',
                                                                                    'data' =>'BLOCKED BY ADMIN ' . $data,
                                                                                    'posted_data' => serialize($params),
                                                                                    'whoadded' => 1 
                                                                                   ) );
}
/* Function to calculate total stremas 
 * @param:
 * $users_video: int, Toral users with video
 * $users_novideo: int, total users withour video
 */
function calculate_total_streams($users_video,$users_novideo) {
  $total = ($users_video + $users_novideo -1)*$users_video;
  return $total;
}
$suscriber_custom_list = new CP_AppBookingPluginSubscribers();

add_action('admin_menu', array($suscriber_custom_list,'admin_menu') );

//add_filter('settings_subscriber_page', array('CP_AppBookingPlugin','settings_subscriber_page') );
//Create new custom instance to allow users to delete their own booking
class  CP_AppBookingPluginSubscribers extends  CP_AppBookingPlugin{
    private $menu_parameter = 'cp_apphourbookings';
    private $prefix = 'cp_appbooking';
    private $plugin_name = 'Mis Resrevas';
    private $plugin_URL = 'https://apphourbooking.dwbooster.com/';
    private $plugin_download_URL = 'https://apphourbooking.dwbooster.com/download';
    public $table_items = "cpappbk_forms";
    public $table_messages = "cpappbk_messages";
    public $print_counter = 1;
    public $plugin_dir = '/wp-content/plugins/appointment-hour-booking';
    private $include_user_data_csv = false;
    private $booking_form_nonce = false;
    public $CP_CFPP_global_templates;
    protected $postURL;

    protected $paid_statuses = array('Pending','Cancelled','Rejected');
    public $shorttag = 'CP_APP_HOUR_BOOKING';
    public $blocked_by_admin_indicator = 'BLOCKED@BY.ADMIN';

    public function admin_menu() {
    global $current_user; 
        wp_get_current_user();
    // Remove Welcome panel.
      if ( !user_can( $current_user, "administrator" ) ){
          add_action('admin_enqueue_scripts', array($this, 'insert_susbcribers_adminScripts'), 1);
	  add_menu_page( $this->plugin_name.' '.__( 'Options' ,'appointment-hour-booking'), $this->plugin_name, 'read', $this->menu_parameter, array($this, 'settings_subs_page') );
      }	
    }

    function insert_susbcribers_adminScripts(){
      wp_enqueue_style('cpapp-newadminstyle', $this->plugin_dir . '/css/newadminlayout.css');
    }

    public function settings_subs_page() {
        global $wpdb;
        if ($this->get_param("cals") || $this->get_param("cals") == '0' || $this->get_param("pwizard") == '1')
        {
            if ($this->get_param("lists") == '1')
                @include_once  '/var/www/html/agenda-bbb.maadix.net/wp-content/themes/27child/cp-admin-int-message-list-subscribers.inc.php';
      } else 
                @include_once '/var/www/html/agenda-bbb.maadix.net/wp-content/themes/27child/cp-admin-int-subscr-list.inc.php';
      
    }

}
