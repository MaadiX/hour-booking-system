

Este desarrollo es una adaptación del plugin [Appointment Hour Booking](https://wordpress.org/plugins/appointment-hour-booking/) implementada en el tema [Twenty Seventeen](https://wordpress.org/themes/twentyseventeen/) de Wordpress que permite ofrecer un sistema de reservas basadas en el día, franja horaria y números de participantes en la sesión.

Concretamente, está orientado a la reserva de sesiones de videollamadas en [_BigBlueButton_](https://bigbluebutton.org/), para lo cual es importante saber el número de participantes con cámara y sin cámara para poder de calcular el número de _streams_ que el servidor va a tener que emitir.

Para aceptar o rechazar una reserva se calcula el número de _streams_ que se prevén tener en una sesión (en un día y franja horaria dado) y se suma con el resto de sesiones ya reservadas. Si la suma final supera el número máximo de _streams_, la reserva es rechazada y te sale el siguiente mensaje de error: "Lo sentimos, con su reserva se supera el máximo número de participantes...". En caso contrario, la reserva se confirma y te dirige a la página indicada en el frontend (Ejemplo: página de "gracias por tu reserva").


Requerimientos:

- [Wordpress](https://wordpress.org/).
- Tema [Twenty Seventeen](https://wordpress.org/themes/twentyseventeen/).
- Plugin [Appointment Hour Booking](https://wordpress.org/plugins/appointment-hour-booking/).

En el archivo funtcions.php se definen los siguientes valores que se tienen que modificar según el entorno:

```
define( 'MAX_STREAMS', 2500 );  // El número máximo se streams permitidos
define( 'MARGEN_VIDEO_USERS', 50 ); // El margen de streams que se quiere reservar para sesiones pequeñas, de forma que no tengan que resrevar
define( 'AUDIO_USERS_FIELDNAME', 'fieldname4' ); // El nombre del campo de formulario de Appointment Hour Booking para particpantes sin vídeo
define( 'VIDEO_USERS_FIELDNAME', 'fieldname3' ); // El nombre del campo de formulario de Appointment Hour Booking para particpantes con vídeo
define( 'MAX_CAPACITY', 10); // El total de slots disponibles. Hay que definirlo tambiñen en el formulario. Sirve para permitir múltiples reservas de un mimso slot. Cuando se alcanza el máximo de streams para un determinado slot se crea una reserva de tipo BLOCKED BY ADMIN, cuya cantidad es el resultado de MAX_CAPACITY - total quantities
```

---


This development is an adaptation of the plugin [Appointment Hour Booking](https://wordpress.org/plugins/appointment-hour-booking/) implemented in the Wordpress theme [Twenty Seventeen](https://wordpress.org/themes/twentyseventeen/) that allows to offer a booking system based on the day, time slot and number of participants in a session.


Specifically, it is oriented to the booking of video call sessions in [_BigBlueButton_](https://bigbluebutton.org/), for which it is important to know the number of participants with camera and without camera in order to calculate the number of _streams_ that the server will have to broadcast.

In order to accept or deny a booking, the number of _streams_ expected to be in a session (on a given day and time slot) is calculated and summed up with the rest of the sessions already booked. If the final amount exceeds the maximum number of _streams_, the booking is rejected and you get the following error message: "Sorry, your booking exceeds the maximum number of participants". Otherwise, the reservation is confirmed and you are directed to the page indicated in the frontend (Example: page of "thank you for your reservation").


Requirements:

- [Wordpress](https://wordpress.org/).
- Wordpress Theme [Twenty Seventeen](https://wordpress.org/themes/twentyseventeen/).
- [Appointment Hour Booking Plugin](https://wordpress.org/plugins/appointment-hour-booking/).

In the funtcions.php file, the following values are defined and may be modified according to the environment:

```
define( 'MAX_STREAMS', 2500 ); // The maximum number of streams allowed
define( 'MARGEN_VIDEO_USERS', 50 ); // The margin of streams that you want to reserve for small sessions, so that  don't need booking
define( 'AUDIO_USERS_FIELDNAME', 'fieldname4' ); // The name of the Appointment Hour Booking form field for participants without video
define( 'VIDEO_USERS_FIELDNAME', 'fieldname3' ); // The name of the Appointment Hour Booking form field for participants with video
defines( 'MAX_CAPACITY', 10); // The total number of slots available. This must also be defined in the form. It serves to allow multiple reservations of the same slot. When the maximum number of streams for a given slot is reached, a reserve of type BLOCKED BY ADMIN is created, whose quantity is the result of MAX_CAPACITY - total quantities
```
