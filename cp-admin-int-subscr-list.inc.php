<?php
/*
if ( !is_admin() )
{
    echo 'Direct access not allowed.';
    exit;
}

*/
$current_user_access = current_user_can('read');

global $wpdb, $cpappb_addons_active_list, $cpappb_addons_objs_list;

$message = "";

?>
<div class="wrap">
<h1><?php echo $this->plugin_name; ?></h1>
<h4><?php _e("Haz clic en el Botón \"Pedidos de Reservas\" del formulario que quieres consultar",'appointment-hour-booking');?>
<?php print($current_user_access);?>
<script type="text/javascript">

 function cp_viewMessages(id)
 {
    document.location = 'admin.php?page=<?php echo $this->menu_parameter; ?>&cals='+id+'&lists=1&r='+Math.random();
 }

 function cp_deleteItem(id)
 {
    if (confirm('Are you sure that you want to delete this item?'))
    {
        document.location = 'admin.php?page=<?php echo $this->menu_parameter; ?>&anonce=<?php echo $nonce; ?>&d='+id+'&r='+Math.random();
    }
 }

</script>
<br>
<div class="ahb-section-container">
	<div class="ahb-section">
  <table cellspacing="10" cellpadding="6" class="ahb-calendars-list">
   <tr>
    <th align="left"><?php _e('ID','appointment-hour-booking'); ?></th><th align="left"><?php _e('Form Name','appointment-hour-booking'); ?></th><th align="left">&nbsp; &nbsp; <?php _e('Options','appointment-hour-booking'); ?></th>
   </tr>
<?php

  $current_user = wp_get_current_user();
  $myrows = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix.$this->table_items);
  foreach ($myrows as $item)
  if ($current_user_access || @in_array($current_user->ID, unserialize($item->cp_user_access)))
  {
?>
   <tr>
    <td nowrap class="firstcol"><?php echo $item->id; ?></td>
    <td nowrap><input type="text" name="calname_<?php echo $item->id; ?>" id="calname_<?php echo $item->id; ?>" value="<?php echo esc_attr($item->form_name); ?>" /></td>

    <td>
<?php if ($current_user_access) { ?> 
                             <input style="margin-bottom:5px;" class="button-primary2 button" type="button" name="calmessages_<?php echo $item->id; ?>" value="<?php _e('Booking Orders','appointment-hour-booking'); ?>" onclick="cp_viewMessages(<?php echo $item->id; ?>);" />
<?php } ?>
    </td>
   </tr>
<?php
   }
?>

  </table>

     <div class="clearer"></div>
     
	</div>
</div>

</form>
</div>
