<?php
/**
 * Template Name: Only Logged user
 * The template for displaying private pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen Child
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
		
               // $roomavailable = $_GET["norooms"]? $_GET["norooms"] : 'false';
		$roomavailable = get_query_var("norooms","false");
                if ($roomavailable=='true'){
                    echo "<div class='alert error'>Lo sentimos, con su reserva se supera el máximo número de participantes para el horario solicitado. Trata de reservar tu sesión para otra hora o reducir el número de personas.</div>";
        }
			while ( have_posts() ) :
				the_post();
                                  get_template_part( 'template-parts/page/contentprivate', 'page' );
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer();
